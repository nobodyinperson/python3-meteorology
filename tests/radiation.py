#!/usr/bin/envpython3

# system modules
import unittest

# internal modules
from meteorology.radiation import blackbody_radiation
from meteorology.radiation import \
    adjust_radiation_temperature_to_other_emissivity as adjtemp
from meteorology.constants import stefan_boltzmann_constant
from .test_flow import *

# external modules
import numpy as np

class BlackbodyRadiationTest(BasicTest):
    def test_saturation_pressure_magnus_over_water(self):
        # numbers
        self.assertFunctionResultsClose(
            [0,stefan_boltzmann_constant,
                315.65740930067255,1099.3727101601216,73487956.32,
                np.array([0,stefan_boltzmann_constant,
                    315.65740930067255,1099.3727101601216,73487956.32])],
            blackbody_radiation,
            [0,1,273.15,373.15,6000,
                np.array([0,1,273.15,373.15,6000])],
            )

class RadiationTemperatureConversionTest(BasicTest):
    def test_conversion_bb_to_gb_temperature_homogeneous(self):
        f = lambda t,e: adjtemp(t,emissivity_old=1,emissivity_new=e,T_ambient=t)
        # if bb temp and ambient temp equal, then reading doesn't change with
        # emissivity
        for t,e in zip(np.linspace(10,1000,10),np.linspace(0.01,1,10)):
            self.assertClose( f(t,e), t )

    def test_conversion_bb_to_gb_temperature_no_ambient(self):
        f = lambda t,e: adjtemp(t,emissivity_old=1,emissivity_new=e)
        # if ambient radiation reflection is ignored, bb temperature is just
        # multiplied by e^(-1/4)
        for t,e in zip(np.linspace(10,1000,10),np.linspace(0.01,1,10)):
            self.assertClose( f(t,e), t / e ** (1/4) )

    def test_conversion_bb_to_gb_temperature_invalid_emissivity(self):
        for t,eo,en in zip(np.linspace(10,1000,10),
            np.array([0.2,0.4,0.6,0.8,1,-1,-0.1,1.1,2]),
            np.array([-1,-0.1,0,1.1,2,0.2,0.4,0.6,0.8,1])):
            self.assertFunctionResultsCloseWithWarning(
                [t],
                [np.nan] * t.size,
                lambda t: adjtemp(t,emissivity_old=eo,emissivity_new=en),
                category = UserWarning,
                )

