#!/usr/bin/env python3

# system modules
import unittest

# internal modules
from meteorology.temperature import *
from .test_flow import *

# external modules
import numpy as np

class TemperatureTest(BasicTest):
    def test_cel2kel_valid(self):
        self.assertFunctionResultsClose(
            [273.15,373.15,473.15,0,np.array([0,273.15,373.15,473.15])],
            cel2kel,
            [0,100,200,-273.15,np.array([-273.15,0,100,200])],
            )

    def test_cel2kel_unphysical_temperatures(self):
        self.assertFunctionResultsCloseWithWarning(
            [-273.15-1e-3,-300,-400,np.array([-273.15-1e-3,-300,-400,])],
            [np.nan] * 3 + [np.array([np.nan] * 3)],
            cel2kel,
            category = UserWarning,
            )

    def test_kel2cel_valid(self):
        self.assertFunctionResultsClose(
            [-273.15,-173.15,-73.15,0,np.array([-273.15,-173.15,0,100])],
            kel2cel,
            [0,100,200,273.15,np.array([0,100,273.15,373.15])],
            )

    def test_kel2cel_unphysical_temperatures(self):
        self.assertFunctionResultsCloseWithWarning(
            [-1e-3,-100,-200,np.array([-1e-3,-100,-200])],
            [np.nan] * 3 + [np.array([np.nan] * 3)],
            kel2cel,
            category = UserWarning,
            )

