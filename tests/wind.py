#!/usr/bin/envpython3
# system modules
import unittest

# internal modules
from meteorology.wind import rad2deg, deg2rad, wind_vector_angle
from .test_flow import *

# external modules
import numpy as np

class AngleConversionTest(BasicTest):
    def setUp(self):
        self.radians = [0,np.pi,2*np.pi,-4,np.array([0,-6])]
        self.degrees = [0,180,360,-4/np.pi*180,np.array([0,-6/np.pi*180])]

    def test_rad2deg(self):
        self.assertFunctionResultsClose(self.degrees, rad2deg, self.radians)

    def test_deg2rad(self):
        self.assertFunctionResultsClose(self.radians, deg2rad, self.degrees)

class WindDirectionTest(BasicTest):
    def setUp(self):
        self.u = [0, 0, 1, 1, -1, -1]
        self.v = [0, 1, 1, -1, -1, 0]
        self.d_origin_clockwise = \
            [np.nan, np.pi, 5 / 4 * np.pi, 7 / 4 * np.pi, np.pi / 4, np.pi / 2]
        self.d_tip_clockwise = \
            [np.nan, 0, np.pi / 4, 3 / 4 * np.pi, 5 / 4 * np.pi, 3 / 2 * np.pi]
        self.d_origin_anticlockwise = \
            [np.nan, np.pi, 3 / 4 * np.pi, np.pi / 4, 7 / 4 * np.pi, 3/2*np.pi]
        self.d_tip_anticlockwise = \
            [np.nan, 0, 7 / 4 * np.pi, 5 / 4 * np.pi, 3 / 4 * np.pi, np.pi / 2]
        # also run the tests with numpy arrays
        for a in (self.u, self.v, self.d_origin_clockwise,
            self.d_tip_clockwise, self.d_origin_anticlockwise,
            self.d_tip_anticlockwise):
            a.append(np.array(a))

    def test_wind_vector_to_direction_origin_clockwise(self):
        with self.assertWarns(UserWarning):
            self.assertFunctionResultsClose(
                self.d_origin_clockwise,
                lambda u,v: wind_vector_angle(u,v,origin=True,clockwise=True),
                self.u,
                self.v,
                )

    def test_wind_vector_to_direction_origin_anticlockwise(self):
        with self.assertWarns(UserWarning):
            self.assertFunctionResultsClose(
                self.d_origin_anticlockwise,
                lambda u,v: wind_vector_angle(u,v,origin=True,clockwise=False),
                self.u,
                self.v,
                )

    def test_wind_vector_to_direction_tip_anticlockwise(self):
        with self.assertWarns(UserWarning):
            self.assertFunctionResultsClose(
                self.d_tip_anticlockwise,
                lambda u,v:wind_vector_angle(u,v,origin=False,clockwise=False),
                self.u,
                self.v,
                )

    def test_wind_vector_to_direction_tip_clockwise(self):
        with self.assertWarns(UserWarning):
            self.assertFunctionResultsClose(
                self.d_tip_clockwise,
                lambda u,v: wind_vector_angle(u,v,origin=False,clockwise=True),
                self.u,
                self.v,
                )
