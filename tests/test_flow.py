#!/usr/bin/env python3

# system modules
import unittest
import warnings

# external modules
import numpy as np

class BasicTest(unittest.TestCase):
    def assertClose(self,a,b):
        np.testing.assert_array_almost_equal(a,b)

    def assertElementsClose(self,l1,l2):
        for a,b in zip(l1,l2):
            self.assertClose(a,b)

    def assertFunctionResultsClose(self,res,fun,*args):
        for d in zip(
                *[(a if hasattr(a,"__iter__") else [a]) for a in args],
                res if hasattr(res,"__iter__") else [res]):
            d = list(d)
            r = d.pop()
            self.assertClose(fun(*d),r)

    def assertFunctionResultsCloseWithWarning(
        self,l1,l2,fun,category=UserWarning):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always") # record all warnings

            nw = len(w) # number of warnings
            for a,b in zip(l1,l2):
                # do the check
                self.assertClose(fun(a),b)
                # check if warning was raised
                self.assertGreater(len(w),nw)
                # check if warning has right category
                self.assertEqual(w[-1].category,category)
                nw = len(w)
