#!/usr/bin/envpython3

# system modules
import unittest
import warnings

# internal modules
from meteorology.humidity import saturation_water_vapour_pressure as e_s
from .test_flow import *

# external modules
import numpy as np

class SaturationWaterVapourPressureTest(BasicTest):
    def test_saturation_pressure_magnus_over_water(self):
        e = lambda t: e_s(t,over="water",param="magnus")

        # numbers
        self.assertFunctionResultsClose(
            [11.1708120600,6.112e2,25146.71439989,
                np.array([11.170812060,6.112e2,25146.71439989])],
            e,
            [273.15-45,273.15,273.15+65,
                np.array([273.15-45,273.15,273.15+65])],
            )

        # check warning is raised outside parameterisation scope
        self.assertFunctionResultsCloseWithWarning(
            [223.15,343.15,373.15,
                np.array([223.15,343.15,373.15])],
            [6.382078, 31397.6752678, 103844.91830878862,
                np.array([6.382078, 31397.6752678, 103844.91830878862])],
            e,
            category=UserWarning,
            )

    def test_saturation_pressure_magnus_over_ice(self):
        e = lambda t: e_s(t,over="ice",param="magnus")

        # numbers
        self.assertFunctionResultsClose(
            [5.40007664e-01,1.65287132e+02,6.11703731e+02,
                np.array([5.40007664e-01,1.65287132e+02,6.11703731e+02,])],
            e,
            [273.15-65,273.15-15,273.15+0.01,
                np.array([273.15-65,273.15-15,273.15+0.01])],
            )

        # check warning is raised outside parameterisation scope
        self.assertFunctionResultsCloseWithWarning(
            [203.15,273.15+10,273.15+20,
                np.array([203.15,273.15+10,273.15+20])],
            [0.2608190924,1353.0693876147,2837.0525289692,
                np.array([0.2608190924,1353.0693876147,2837.0525289692])],
            e,
            category=UserWarning
            )

    def test_saturation_pressure_magnus_auto(self):
        e = lambda t: e_s(t,over="auto",param="magnus")

        # numbers
        self.assertFunctionResultsClose(
            [5.40007664e-01,6.112e2,25146.71439989,
                np.array([5.40007664e-01,6.112e2,25146.71439989])],
            e,
            [273.15-65,273.15,273.15+65,
                np.array([273.15-65,273.15,273.15+65])],
            )

        # check warning is raised outside parameterisation scope
        self.assertFunctionResultsCloseWithWarning(
            [203.15,343.15,373.15,
                np.array([203.15,343.15,373.15,])],
            [0.2608190924,31397.6752678, 103844.91830878862,
                np.array([0.2608190924,31397.6752678, 103844.91830878862])],
            e,
            category=UserWarning
            )

