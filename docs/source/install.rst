
Installation
============

:mod:`meteorology` is best installed via ``pip``::

    pip3 install --user meteorology
